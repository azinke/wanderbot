/**
 * @date: 04.06.2021
 * @summary: Move the turtlebot3 robot around (without specific goal).
 */
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>

#define QUEUE_SIZE  2

#define PUB_TOPIC "cmd_vel"

#define SUB_TOPIC "scan"

// Distance
#define COLISION_MARGING  0.4 // 40 cm

// Linear speed of the robot
#define LINEAR_SPEED  0.2 // m/s

// Angular speed of the robot
#define ANGULAR_SPEED  0.3 // m/s

#define FREQUENCY 10  // 10Hz


class Cleaner {
  private:
    ros::NodeHandle nh;
    // Publisher
    ros::Publisher pub;

    // Subscriber
    ros::Subscriber sub;

    geometry_msgs::Twist speed;

    bool direction = false;

  public:
    Cleaner() {
      pub = nh.advertise<geometry_msgs::Twist>(PUB_TOPIC, QUEUE_SIZE);
      sub = nh.subscribe(SUB_TOPIC, QUEUE_SIZE, &Cleaner::core_callback,this);
    }

    /**
    * Move the robot around
    * :param msg: Laser readings
    */
    void core_callback(const sensor_msgs::LaserScan::ConstPtr& scan) {
      ros::Rate rate(FREQUENCY);
      // Measurement just in front of the robot
      float distance = scan->ranges[0];
      if (direction) distance = scan->ranges[180];
      ROS_INFO("Distance: %f", distance);
      if (distance <= COLISION_MARGING) {
        // Change direction
        // speed.linear.x = 0;
        direction = !direction;
        if (direction) speed.linear.x = -LINEAR_SPEED;
        speed.angular.z = ANGULAR_SPEED;
      } else {
        // Drive forward
        if (direction) speed.linear.x = -LINEAR_SPEED;
        else speed.linear.x = LINEAR_SPEED;
        speed.angular.z = 0;
      }
      pub.publish(speed);
      ROS_INFO("x: %f, z: %f", speed.linear.x, speed.angular.z);
      rate.sleep();
    }
};

int main(int argc, char **argv) {
  // Initialize the node
  ros::init(argc, argv, "cleaner");

  Cleaner bot;

  ros::spin();
  return 0;
}
