# Wanderbot

The robot is expected to be wandering around the building. A typical application
would be a cleaning robot.

Make sure that the docker container mentioned i the top `README.md` file is running.


## Run

Spawn first by launching the simulation environment

```bash
roslaunch wanderbot world.launch
```

At this point, you need to restart the docker container. Just stop it
with `CTRL+C` and start it over with:

```bash
docker-compose -f docker/docker-compose.yml up
```

Then you can see the environment properly spawned.
