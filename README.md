# embedded-project

Embedded computing project

## Usage

First the docker container must be build and pushed upstream. In order to build the docker
image, the following command must be issued after successfuly login:

```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/azinke/wanderbot ./docker

docker push registry.gitlab.com/azinke/wanderbot
```

The container contains the `Gazebo` simulatior as well as `RViz`. Those apps need a graphical
server. As so, it's important to start a `X-Server` with the following command:

```bash
xhost local:root
```

If you didn't build the docker image yourself, you need to pull the image first. Since a version
of it is probably already upstream.

```bash
docker pull registry.gitlab.com/azinke/wanderbot:latest
```

Then, start the docker container using `docker-compose`

```bash
docker-compose -f docker/docker-compose.yml up
```

The `Gazebo` simulator will start if everything works properly.

If you need to access `rviz`, you need to go inside the container and start it manually.

```bash
docker exec -it <container-name> /bin/bash
```
